package com.geekslife.kptest.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.geekslife.kptest.controller.SupportInfoController;
import com.geekslife.kptest.model.RegionInfo;
import com.geekslife.kptest.model.SupportInfo;
import com.geekslife.kptest.dto.SupportInfoDTO;
import com.geekslife.kptest.repository.RegionInfoRepository;
import com.geekslife.kptest.repository.SupportInfoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DataService {

    static final Logger logger = LoggerFactory.getLogger(DataService.class);

    @Autowired
    RegionInfoRepository regionInfoRepository;

    @Autowired
    SupportInfoRepository supportInfoRepository;

    /**
     *
     * 지자체 정보를 생성한다.
     *
     * @// TODO: 2019-03-23 지자체 코드 생성 방법/관리법 미정의. 일단 임의 생성.
     * @return 새로 입력된 지자체 데이터 수
     */
    public int importRegionInfo(InputStream inputStream, boolean skipHeader, Charset charset) throws IOException {

        logger.debug("importRegionInfo: skipHeader={}, charset={}", skipHeader, charset);
        int count = 0;

        List<RegionInfo> regionInfos = new ArrayList<>();

        for ( SupportInfoDTO dto : parseCsv(inputStream, skipHeader, charset) ) {
            String regionName = dto.getRegion();
            if (regionInfoRepository.findByRegionName( regionName ) == null) {
                String regionCode = String.format("region_%03d",Long.valueOf(dto.getId()));
                count++;
                regionInfos.add(new RegionInfo(regionCode, regionName));
            }
        }

        regionInfoRepository.saveAll(regionInfos);

        return count;
    }

    public RegionInfo findRegionInfoByRegionName(String regionName) {

        logger.debug("findRegionInfoByRegionName : regioinName={}", regionName);
        return regionInfoRepository.findByRegionName(regionName);
    }

    public String findRegionOrderByAvgRangeAsc() {

        logger.debug("findRegionOrderByAvgRangeAsc");
        return supportInfoRepository.findByMinimumRate();
    }

    @Value("${recommend.server.url}")
    String recoServerUrl;

    @Autowired
    RestTemplate restTemplate;

    public SupportInfoDTO recommend(String text) throws IOException {

        logger.debug("recommend : {}", text);

        Map<String, String> request = new HashMap<>();
        request.put("text",text);

        ResponseEntity<String> response = restTemplate.postForEntity(recoServerUrl, request, String.class);

        JsonNode root = new ObjectMapper().readTree(response.getBody());
        String region = root.get("region").textValue();

        logger.debug("region = {}", region);
        return convertSupportInfo(
                findSupportInfoByRegionName(region));
    }

    public int importRegionInfo(InputStream inputStream) throws IOException {
        return importRegionInfo(inputStream, true, Charset.forName("EUC-KR"));
    }

    public int importSupportInfo(InputStream inputStream) throws  IOException {

        int count=0;

        List<SupportInfo> supportInfos = new ArrayList<>();

        for (SupportInfoDTO dto : parseCsv(inputStream)) {
            supportInfos.add(convertSupportInfoDTO( dto ));
            count++;
        }
        supportInfoRepository.saveAll(supportInfos);

        logger.debug("importSupportInfo : count={}", count);

        return count;
    }

    public List<SupportInfo> findAllSupportInfos() {
        return supportInfoRepository.findAll();
    }

    public List<RegionInfo> findAllRegionInfos() {
        return regionInfoRepository.findAll();
    }

    public SupportInfo findSupportInfoByRegionName(String regionName) {

        RegionInfo regionInfo = regionInfoRepository.findByRegionName(regionName);
        logger.debug("findSupportInfoByRegionName({})={}", regionName, regionInfo);
        if (regionInfo !=null) {
            return supportInfoRepository.findByRegionCode(regionInfo.getRegionCode());
        } else {
            return null;
        }
    }

    public List<String> findRegionByLimitAndRateWithTopN(int k) {

        logger.debug("findRegionByLimitAndRateWithTopN({})", k);
        return supportInfoRepository.findRegionByLimitAndRateWithTopN(k);
    }

    public List<SupportInfo> findOrderedSupportInfos(int k) {

        logger.debug("findOrderedSupportInfos({})", k);
        return supportInfoRepository.findByLimitAndRateWithTopN(k);
    }

    List<SupportInfoDTO> parseCsv(InputStream inputStream) throws IOException {
        return parseCsv(inputStream, true, Charset.forName("EUC-KR"));
    }

    List<SupportInfoDTO> parseCsv(InputStream inputStream, boolean skipHeader, Charset charset) throws IOException {

        logger.debug("parseCsv(skipHeader={}, charset={})", skipHeader, charset);
        MappingIterator<SupportInfoDTO> mappingIterator =
                new CsvMapper().readerWithSchemaFor(SupportInfoDTO.class).readValues(
                        new InputStreamReader(inputStream, charset));
        if (skipHeader)
            mappingIterator.next(); // skip header

        return mappingIterator.readAll();
    }

    public SupportInfo convertSupportInfoDTO(SupportInfoDTO dto) {

        SupportInfo info = new SupportInfo();

        BeanUtils.copyProperties(dto, info, "id", "region", "limit", "rate");

        info.setId(Long.valueOf(dto.getId()));
        info.setLimit(SupportInfoDTO.parseLimit(dto.getLimit()));
        info.setRate(SupportInfoDTO.parseRate(dto.getRate()));

        String regionCode = regionInfoRepository.findByRegionName( dto.getRegion() ).getRegionCode();
        info.setRegionCode( regionCode );

        logger.debug("converted SupportInfo = {}", info);
        return info;
    }

    public SupportInfoDTO convertSupportInfo(SupportInfo info) {

        SupportInfoDTO dto = new SupportInfoDTO();

        BeanUtils.copyProperties(info, dto, "id", "region", "limit", "rate");

        dto.setId(String.valueOf(info.getId()));
        dto.setLimit(SupportInfoDTO.toLimit(info.getLimit()));
        dto.setRate(SupportInfoDTO.toRate(info.getRate()));

        RegionInfo regionInfo = regionInfoRepository.findById(info.getRegionCode()).get();
        dto.setRegion(regionInfo.getRegionName());

        logger.debug("converted SupportInfoDTO = {}", dto);
        return dto;
    }

    public void save(SupportInfo info) {
        supportInfoRepository.save(info);
    }

    @Transactional
    public void deleteAll() {
        supportInfoRepository.deleteAll();
        regionInfoRepository.deleteAll();
    }
}