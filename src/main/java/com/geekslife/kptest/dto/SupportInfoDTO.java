package com.geekslife.kptest.dto;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonView;
import com.geekslife.kptest.controller.View;
import com.geekslife.kptest.model.Range;
import lombok.Data;
import org.thymeleaf.util.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Data
@JsonPropertyOrder({"id", "region", "target", "usage", "limit", "rate", "institute", "mgmt", "reception"})
public class SupportInfoDTO {
    String id;          // 구분
    @JsonView({ View.Visible.class, View.Reco.class })
    String region;      // 지자체명(기관명)
    @JsonView(View.Visible.class)
    String target;      // 지원대상
    @JsonView({ View.Visible.class, View.Reco.class })
    String usage;       // 용도
    @JsonView({ View.Visible.class, View.Reco.class })
    String limit;       // 지원한도
    @JsonView({ View.Visible.class, View.Reco.class })
    String rate;        // 이차보전
    @JsonView(View.Visible.class)
    String institute;   // 추천기관
    @JsonView(View.Visible.class)
    String mgmt;        // 관리점
    @JsonView(View.Visible.class)
    String reception;   // 취급점

    public static String toRate(Range range) {
        if (range.getFrom() == 100.f && range.getTo() == 100.f) {
            return "대출이자 전액";
        } else if (range.getFrom() == range.getTo()) {
            return String.format("%.2f%%", range.getFrom());
        } else {
            return String.format("%.2f%%~%.2f%%",range.getFrom(),range.getTo());
        }
    }

    public static Range parseRate(String rate) {

        if (!StringUtils.isEmpty(rate)) {

            if (rate.matches("\\d+\\.?\\d*%")) {    // 3%
                float r = Float.parseFloat(rate.replace("%",""));
                return Range.create(r,r);
            } else if (rate.matches( "\\d+\\.?\\d*%?~\\d+\\.?\\d*%")) {  // 2.5%~3%
                String[] r = rate.replace("%","").split("~");
                float from = Float.parseFloat(r[0]);
                float to = Float.parseFloat(r[1]);
                return Range.create(from, to);
            } else if (rate.equals("대출이자 전액")) {
                return Range.create(100f, 100f);
            }
        }
        throw new IllegalArgumentException("Cannot parse rate : "+rate);
    }

    public static String toLimit(Long limit) {
        if (limit == 0) {
            return "추천금액 이내";
        } else if (limit>=100_000_000) {
            return String.format("%d억 이내", limit/100_000_000);
        } else {
            return String.format("%d백만원 이내", limit/1_000_000);
        }
    }

    public static long parseLimit(String limit) {

        if(!StringUtils.isEmpty(limit)) {

            Pattern limitPattern = Pattern.compile("(\\d+)(억|백만)원?\\s*이내");
            Matcher m = limitPattern.matcher(limit);

            if (limit.equals("추천금액 이내")) {
                return 0;
            } else if (m.matches()) {
                long unit = m.group(2).equals("억") ? 100_000_000 : 1_000_000;
                return unit * Long.valueOf( m.group(1) );
            }
        }
        throw new IllegalArgumentException("Cannot parse limit : "+limit);
    }
}
