package com.geekslife.kptest.model;

import lombok.Data;
import lombok.Getter;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.usertype.UserType;
import org.springframework.util.ObjectUtils;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Objects;

@Data
public class Range implements UserType {

    float from;

    float to;

    float avg;

    public static Range create(float from, float to) {
        Range r = new Range();
        r.from = from;
        r.to = to;
        r.avg = (from+to)/2.0f;
        return r;
    }

    @Override
    public int[] sqlTypes() {
        return new int[] { Types.FLOAT, Types.FLOAT, Types.FLOAT };
    }

    @Override
    public Class returnedClass() {
        return Range.class;
    }

    @Override
    public boolean equals(Object x, Object y) throws HibernateException {

        return ObjectUtils.nullSafeEquals(x,y);
    }

    @Override
    public int hashCode(Object x) throws HibernateException {

        assert (x!=null);
        return x.hashCode();
    }

    @Override
    public Object nullSafeGet(ResultSet rs, String[] names, SharedSessionContractImplementor session, Object owner) throws HibernateException, SQLException {

        if (rs.wasNull())
            return null;

        float from = rs.getFloat(names[0]);
        float to = rs.getFloat(names[1]);

        Range r = new Range();
        r.from = from;
        r.to = to;
        r.avg = (from+to)/2;
        return r;
    }

    @Override
    public void nullSafeSet(PreparedStatement st, Object value, int index, SharedSessionContractImplementor session) throws HibernateException, SQLException {

        if (Objects.isNull(value)) {
            st.setNull(index, Types.FLOAT);
        } else {
            Range range = (Range) value;
            st.setFloat(index, range.from);
            st.setFloat(index+1, range.to);
            st.setFloat(index+2, range.avg);
        }
    }

    @Override
    public Object deepCopy(Object value) throws HibernateException {
        return value;
    }

    @Override
    public boolean isMutable() {
        return false;
    }

    @Override
    public Serializable disassemble(Object value) throws HibernateException {
        return (Serializable) value;
    }

    @Override
    public Object assemble(Serializable cached, Object owner) throws HibernateException {
        return cached;
    }

    @Override
    public Object replace(Object original, Object target, Object owner) throws HibernateException {
        return original;
    }
}
