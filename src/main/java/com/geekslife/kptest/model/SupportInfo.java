package com.geekslife.kptest.model;

import lombok.Data;
import org.hibernate.annotations.Columns;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name="SUPPORT_INFO")
public class SupportInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    // 지자체 코드
    @JoinColumn(table="REGION_INFO", name="REGION_CODE")
    String regionCode;

    // 지원 대상
    @Column(name="TARGET")
    String target;

    // 용도
    @Column(name="USAGE")
    String usage;

    // 지원 한도. 0 인 경우 추천 금액 이내
    @Column(name="SUPPORT_LIMIT")
    Long limit;

    // 이차보전
    @Type(type="com.geekslife.kptest.model.Range")
    @Columns(columns = { @Column(name="RATE_FROM"), @Column(name="RATE_TO"), @Column(name="RATE_AVG") })
    Range rate;

    // 추천기관
    @Column(name="INSTITUTE")
    String institute;

    // 관리점
    @Column(name="MGMT")
    String mgmt;

    // 취급점
    @Column(name="RECEPTION")
    String reception;

    // 생성일자
    @Column(name="CREATED_AT")
    Date creationTime;

    // 수정일자
    @Column(name="MODIFIED_AT")
    Date modificationTime;

    @PrePersist
    void onCreate() {
        creationTime = modificationTime = new Date();
    }

    @PreUpdate
    void onUpdate() {
        modificationTime = new Date();
    }
}
