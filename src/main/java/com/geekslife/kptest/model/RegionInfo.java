package com.geekslife.kptest.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name="REGION_INFO")
public class RegionInfo {

    public RegionInfo() {}

    public RegionInfo(String regionCode, String regionName) {
        this.regionCode = regionCode;
        this.regionName = regionName;
    }

    @Id
    @Column(name="REGION_CODE")
    String regionCode;

    @Column(name="REGION_NAME")
    String regionName;
}
