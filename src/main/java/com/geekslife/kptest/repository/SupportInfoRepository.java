package com.geekslife.kptest.repository;

import com.geekslife.kptest.model.SupportInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SupportInfoRepository extends JpaRepository<SupportInfo, Long> {

    SupportInfo findByRegionCode(String regionCode);

    @Query(value="SELECT * FROM SUPPORT_INFO ORDER BY SUPPORT_LIMIT DESC,RATE_AVG ASC LIMIT ?#{[0]}", nativeQuery=true)
    List<SupportInfo> findByLimitAndRateWithTopN(@Param("SIZE") int size);

    @Query(value="SELECT REGION_NAME FROM REGION_INFO as r JOIN SUPPORT_INFO as s ON r.REGION_CODE=s.REGION_CODE ORDER BY SUPPORT_LIMIT DESC,RATE_AVG ASC  LIMIT ?#{[0]}", nativeQuery=true)
    List<String> findRegionByLimitAndRateWithTopN(@Param("SIZE") int size);

    @Query(value="SELECT REGION_NAME FROM REGION_INFO as r JOIN SUPPORT_INFO as s ON r.REGION_CODE=s.REGION_CODE ORDER BY RATE_AVG ASC LIMIT 1", nativeQuery=true)
    String findByMinimumRate();
}
