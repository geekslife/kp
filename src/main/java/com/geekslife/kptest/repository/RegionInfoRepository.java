package com.geekslife.kptest.repository;

import com.geekslife.kptest.model.RegionInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RegionInfoRepository extends JpaRepository<RegionInfo, String> {

    RegionInfo findByRegionName(String regionName);
}
