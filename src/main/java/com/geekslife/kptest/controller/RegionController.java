package com.geekslife.kptest.controller;

import com.fasterxml.jackson.annotation.JsonView;
import com.geekslife.kptest.dto.SupportInfoDTO;
import com.geekslife.kptest.model.RegionInfo;
import com.geekslife.kptest.security.WebSecurityConfig;
import com.geekslife.kptest.service.DataService;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.MissingFormatArgumentException;

@Api(description="지자체명 조회 및 조건에 따른 정렬 및 필터링")
@RestController
@RequestMapping("/api/v1/region")
public class RegionController {

    static final Logger logger = LoggerFactory.getLogger(RegionController.class);

    @Autowired
    DataService dataService;

    @ApiOperation(value="지자체 코드와 지자체 이름 매핑 정보 조회")
    @GetMapping("/all")
    public List<RegionInfo> listAllRegionInfo(
            @RequestHeader(value = WebSecurityConfig.AUTHORIZATION_HEADER_NAME,required = false) String authHeader) {
        return dataService.findAllRegionInfos();
    }

    @ApiOperation(value="지원한도 컬럼에서 지원금액으로 내림차순 정렬(지원금액이 동일하면 이차보전 평균 비율이 적은 순서)하여 특성 개수만 출력")
    @ApiImplicitParams({
            @ApiImplicitParam(
                    name="params", value = "Example Value 에 다음과 같이 JSON 형태로 Size 를 명시한다:\n{\"size\":\"12\"} ", required = true, paramType = "body",
                    examples = @Example(
                            value = {
                                @ExampleProperty(value = "0")
                            })
            )
    })
    @PostMapping("/sorted")
    public List<String> listOrderByLimitAndRateDescBySize(
            @RequestHeader(value = WebSecurityConfig.AUTHORIZATION_HEADER_NAME,required = false) String authHeader,
            @RequestBody Map<String, String> params) {

        logger.debug("listOrderByLimitAndRateDescBySize : {}", params);

        int k = Integer.valueOf(params.get("size"));

        return dataService.findRegionByLimitAndRateWithTopN(k);
    }

    @ApiOperation(value="이차보전 컬럼에서 보전 비율이 가장 작은 추천 기관명을 출력")
    @PostMapping("/min-rate")
    public String getRegionNameOfMinimumRate(
            @RequestHeader(value = WebSecurityConfig.AUTHORIZATION_HEADER_NAME,required = false) String authHeader
    ) {
        return dataService.findRegionOrderByAvgRangeAsc();
    }

    @JsonView(View.Reco.class)
    @PostMapping("/reco")
    public SupportInfoDTO recommend(
            @RequestHeader(value = WebSecurityConfig.AUTHORIZATION_HEADER_NAME,required = false) String authHeader,
            @RequestBody Map<String, String> query) throws IOException {

        logger.debug("recommend : {}", query);
        if (query.containsKey("text")) {
            return dataService.recommend(query.get("text"));
        } else {
            throw new MissingFormatArgumentException("뉴스 텍스트(\"text\") 필드 필수");
        }
    }
}
