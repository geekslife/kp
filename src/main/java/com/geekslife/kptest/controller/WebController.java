package com.geekslife.kptest.controller;

import com.geekslife.kptest.dto.SupportInfoDTO;
import com.geekslife.kptest.repository.RegionInfoRepository;
import com.geekslife.kptest.repository.SupportInfoRepository;
import com.geekslife.kptest.service.DataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;
import java.util.stream.Collectors;

@ApiIgnore
@Controller
@RequestMapping("/")
public class WebController {

    @Autowired
    RegionInfoRepository regionInfoRepository;

    @Autowired
    SupportInfoRepository supportInfoRepository;

    @Autowired
    DataService dataService;

    @GetMapping("/admin")
    public String index(Model model) {

        model.addAttribute( "regionInfo", regionInfoRepository.findAll() );

        List<SupportInfoDTO> dtoList = dataService.findAllSupportInfos().stream()
                .map( dataService::convertSupportInfo )
                .collect( Collectors.toList() );
        model.addAttribute( "supportInfo", dtoList);

        return "admin";
    }

    @GetMapping("/upload-form")
    public String uploadForm() {
        return "upload-form";
    }
}
