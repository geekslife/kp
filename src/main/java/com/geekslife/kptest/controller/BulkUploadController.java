package com.geekslife.kptest.controller;

import com.geekslife.kptest.security.WebSecurityConfig;
import com.geekslife.kptest.service.DataService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Api(description="데이터 업로드 및 레코드 등록 처리")
@RestController
@RequestMapping(value = "/api/v1/upload", produces = "application/json")
public class BulkUploadController {

    static final Logger logger = LoggerFactory.getLogger(BulkUploadController.class);

    @Autowired
    DataService dataService;

    @ApiOperation(value="지자체 코드-이름 매핑정보", hidden = true)
    @PostMapping("/region-info")
    public String handleRegionInfo(
            @RequestHeader(value = WebSecurityConfig.AUTHORIZATION_HEADER_NAME, required = false) String authHeader,
            @RequestParam("file") MultipartFile file) throws IOException {

        int count = dataService.importRegionInfo( file.getInputStream() );

        logger.debug("handleRegionInfo with {} rows updated.", count);

        return String.valueOf(count);
    }

    @ApiOperation(value="데이터 파일에서 각 레코드를 데이터베이스에 저장")
    @PostMapping("/support-info")
    public String handleSupportInfo(
            @RequestHeader(value = WebSecurityConfig.AUTHORIZATION_HEADER_NAME,required = false) String authHeader,
            @RequestParam("file") MultipartFile file) throws  IOException {

        dataService.importRegionInfo( file.getInputStream() );

        int count = dataService.importSupportInfo( file.getInputStream() );

        logger.debug("handleSupportInfo with {} rows updated.", count);

        return String.valueOf(count);
    }
}