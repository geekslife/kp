package com.geekslife.kptest.controller;

import com.fasterxml.jackson.annotation.JsonView;
import com.geekslife.kptest.model.SupportInfo;
import com.geekslife.kptest.dto.SupportInfoDTO;
import com.geekslife.kptest.security.WebSecurityConfig;
import com.geekslife.kptest.service.DataService;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.thymeleaf.util.StringUtils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import java.util.MissingFormatArgumentException;
import java.util.stream.Collectors;

@Api(description="지자체 지원 정보의 조회 및 관리")
@RestController
@RequestMapping("/api/v1/support-info")
public class SupportInfoController {

    static final Logger logger = LoggerFactory.getLogger(SupportInfoController.class);

    @Autowired
    DataService dataService;

    @JsonView(View.Visible.class)
    @ApiOperation(value="지원하는 지자체명을 입력받아 해당 지역의 지원 정보를 출력")
    @ApiImplicitParams({
            @ApiImplicitParam(
                    name="query", value = "Example Value 에 다음 예와 같이 지역명을 JSON 형태로 입력한다: \n{\"region\":\"강원도\"} ", required = true, paramType = "body",
                    examples = @Example(
                            value = {
                                    @ExampleProperty(value = "0")
                            })
            )
    })
    @PostMapping("/search")
    public SupportInfoDTO search(
            @RequestHeader(value = WebSecurityConfig.AUTHORIZATION_HEADER_NAME,required = false) String authHeader,
            @RequestBody Map<String,String> query) {

        logger.debug("search: query={}",query);
        if (query.containsKey("region")) {
            SupportInfo info = dataService.findSupportInfoByRegionName(query.get("region"));
            return dataService.convertSupportInfo(info);
        } else {
            return null;
        }
    }

    @ApiOperation(value="지원하는 지자체 목록 검색")
    @JsonView(View.Visible.class)
    @GetMapping("/all")
    public List<SupportInfoDTO> listAll(
            @RequestHeader(value = WebSecurityConfig.AUTHORIZATION_HEADER_NAME,required = false) String authHeader
    ) {

        logger.debug("listAll()");
        return dataService.findAllSupportInfos()
                .stream()
                .map( dataService::convertSupportInfo )
                .collect(Collectors.toList());
    }

    @ApiOperation(value="지원하는 지자체 정보 수정")
    @ApiImplicitParams({
            @ApiImplicitParam(
                    name="query", value = "지자체명(region)을 명시하고, 수정할 속성명과 값을 입력하면 해당 속성이 변경된다.\n"+
                    "예를 들어, 다음 예의 경우 강원도의 지원 항목 중 지원한도를 3억원 이내로 변경한다\n"+
                    "{\"region\":\"강원도\", \"limit\":\"3억원 이내\"}", required = true, paramType = "body"
            )
    })
    @PostMapping("/")
    public SupportInfoDTO update(
            @RequestHeader(value = WebSecurityConfig.AUTHORIZATION_HEADER_NAME,required = false) String authHeader,
            @RequestBody Map<String,String> query) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {

        logger.debug("update: query={}", query);
        if (query.containsKey("region")) {
            SupportInfo info = dataService.findSupportInfoByRegionName(query.get("region"));
            SupportInfoDTO dtd = dataService.convertSupportInfo(info);

            for (Field field : SupportInfoDTO.class.getDeclaredFields()) {
                String name = field.getName();

                if (!name.equals("region") && query.containsKey(name)) {
                    String setter = "set"+StringUtils.capitalize(name);
                    Method m = SupportInfoDTO.class.getDeclaredMethod(setter, String.class);
                    m.invoke(dtd, query.get(name));
                }
            }
            info = dataService.convertSupportInfoDTO(dtd);
            dataService.save(info);
            return dtd;
        } else {
            throw new MissingFormatArgumentException("지자체 이름(\"region\") 필드 필수");
        }
    }

}
