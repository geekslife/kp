package com.geekslife.kptest.security.dto;

import lombok.Data;

@Data
public class UserDTO {

    String username;
    String password;
}