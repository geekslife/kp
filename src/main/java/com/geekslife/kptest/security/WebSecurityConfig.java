package com.geekslife.kptest.security;

import com.geekslife.kptest.security.auth.JwtAuthenticationEntryPoint;
import com.geekslife.kptest.security.auth.JwtAuthenticationProvider;
import com.geekslife.kptest.security.auth.JwtAuthenticationTokenFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    static final Logger logger = LoggerFactory.getLogger(WebSecurityConfig.class);

    public static final String AUTHORIZATION_HEADER_NAME = "Authorization";

    public static final String SIGNIN_TOKEN_URL = "/auth/signin";
    public static final String SIGNUP_TOKEN_URL = "/auth/signup";
    public static final String REFRESH_TOKEN_URL = "/auth/refresh";
    public static final String API_ROOT_URL = "/api/**";

    @Autowired
    JwtAuthenticationProvider authenticationProvider;

    @Autowired
    JwtAuthenticationEntryPoint entryPoint;

    @Autowired
    JwtSettings jwtSettings;

    @Bean
    public AuthenticationManager authenticationManager() {
        return new ProviderManager(Collections.singletonList(authenticationProvider));
    }

    @Bean
    public JwtAuthenticationTokenFilter authenticationTokenFilter() {

        JwtAuthenticationTokenFilter filter;

        if (jwtSettings.getSecurityEnabled()) {
            filter = new JwtAuthenticationTokenFilter();
        } else {
            filter = new JwtAuthenticationTokenFilter(false);
        }

        filter.setAuthenticationManager(authenticationManager());
        filter.setAuthenticationSuccessHandler(new AuthenticationSuccessHandler() {
            @Override
            public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                                Authentication authentication) throws IOException, ServletException {}
        });
        return filter;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
       List<String> permitAllEndpointList = Arrays.asList(
                SIGNIN_TOKEN_URL,
                SIGNUP_TOKEN_URL,
                REFRESH_TOKEN_URL,
                "/h2-console",
                "/admin"
        );

        logger.info("security enabled : {}" , jwtSettings.getSecurityEnabled() );

        if (jwtSettings.getSecurityEnabled()) {
            http.csrf().disable()
                    .authorizeRequests().antMatchers(permitAllEndpointList.toArray(new String[permitAllEndpointList.size()])).permitAll()
                    .and()
                    .authorizeRequests().antMatchers(API_ROOT_URL).authenticated()
                    .and()
                    .exceptionHandling().authenticationEntryPoint(entryPoint)
                    .and()
                    .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        } else {
            http.csrf().disable()
                    .authorizeRequests().antMatchers(permitAllEndpointList.toArray(new String[permitAllEndpointList.size()])).permitAll()
                    .and()
                    .authorizeRequests().antMatchers(API_ROOT_URL).permitAll()
                    .and()
                    .exceptionHandling().authenticationEntryPoint(entryPoint)
                    .and()
                    .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        }

        http.headers().frameOptions().disable();
        http.addFilterBefore(authenticationTokenFilter(), UsernamePasswordAuthenticationFilter.class);
        http.headers().cacheControl();
    }
}
