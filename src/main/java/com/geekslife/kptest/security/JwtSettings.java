package com.geekslife.kptest.security;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


@Data
@Component
public class JwtSettings {

    @Value("${security.jwt.accessTokenExpirationTime}")
    Integer accessTokenExpirationTime;

    @Value("${security.jwt.tokenIssuer}")
    String tokenIssuer;

    @Value("${security.jwt.tokenSigningKey}")
    String tokenSigningKey;

    @Value("${security.jwt.refreshTokenExpirationTime}")
    Integer refreshTokenExpirationTime;

    @Value("${security.enabled}")
    Boolean securityEnabled;
}
