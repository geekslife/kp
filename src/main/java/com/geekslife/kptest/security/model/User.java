package com.geekslife.kptest.security.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(name="USER")
public class User {

    @Id
    @Column(name="USERNAME")
    String username;

    @Column(name="PASSWORD")
    String password;

    public User() {}

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    @Column(name="CREATED_AT")
    Date creationTime;

    @PrePersist
    void onCreate() {
        creationTime = new Date();
    }
}
