package com.geekslife.kptest.security.service;

import com.geekslife.kptest.security.model.User;
import com.geekslife.kptest.security.repository.UserRepository;
import com.geekslife.kptest.security.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    public void saveUserInfo(UserDTO userDTO) {
        userRepository.save(new User(userDTO.getUsername(), BCrypt.hashpw(userDTO.getPassword(), BCrypt.gensalt(10))));
    }
}