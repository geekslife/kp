package com.geekslife.kptest.security.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.geekslife.kptest.security.JwtSettings;
import com.geekslife.kptest.security.model.User;
import com.geekslife.kptest.security.util.JwtUtils;
import com.geekslife.kptest.security.auth.JwtTokenFactory;
import com.geekslife.kptest.security.WebSecurityConfig;
import com.geekslife.kptest.security.dto.UserDTO;
import com.geekslife.kptest.security.service.UserService;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(description="사용자 인증 및 토큰 처리")
@RestController
@RequestMapping("/auth")

public class AuthController {

    static final Logger logger = LoggerFactory.getLogger(AuthController.class);

    @Autowired
    JwtTokenFactory jwtTokenFactory;

    @Autowired
    UserService userService;

    @ApiOperation(value="사용자 로그인")
    @PostMapping("/signin")
    public String singIn(@RequestBody UserDTO userDTO) throws JsonProcessingException {

        logger.info("signin > username : {}, password : {} " , userDTO.getUsername(), userDTO.getPassword());

        User user = userService.findByUsername(userDTO.getUsername());

        if (user != null) {

            String inputPassword = userDTO.getPassword();
            String userPassword = user.getPassword();

            if (!BCrypt.checkpw(inputPassword, userPassword)) {
                logger.debug("Password does not match.");
                throw new BadCredentialsException("Password does not match.");
            }
        } else {
            throw new UsernameNotFoundException("Username does not exist.");
        }

        return jwtTokenFactory.createToken(userDTO);
    }

    @ApiOperation(value="사용자 가입")
    @PostMapping("/signup")
    public String signUp(@RequestBody UserDTO userDTO) throws JsonProcessingException {

        logger.debug("signup > username : {}, password : {} ", userDTO.getUsername(), userDTO.getPassword());

        String username = userDTO.getUsername();
        String password = userDTO.getPassword();

        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password)) {
            throw new BadCredentialsException("Username or password is empty.");
        }

        User user = userService.findByUsername(username);

        if (user != null) {
            throw new BadCredentialsException("Username already exists.");
        }

        userService.saveUserInfo(userDTO);

        return jwtTokenFactory.createToken(userDTO);
    }

    @Autowired
    JwtSettings jwtSettings;

    @ApiOperation(value="액세스 토큰 재발급 요청. Authorization 값으로 'Bearer '에 리프레시 토큰을 붙여 전송한다.")
    @PostMapping(value="/refresh")
    public String refresh(@RequestHeader(value=WebSecurityConfig.AUTHORIZATION_HEADER_NAME) String authHeader) {

        String authToken = JwtUtils.extractToken(authHeader);

        UserDTO userDTO = JwtUtils.validate(authToken, jwtSettings.getTokenSigningKey(), true);

        return jwtTokenFactory.createJwtAccessToken(userDTO);
    }
}
