package com.geekslife.kptest.security.util;

import com.geekslife.kptest.security.auth.JwtTokenFactory;
import com.geekslife.kptest.security.dto.UserDTO;
import com.geekslife.kptest.service.DataService;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.stream.Collectors;

public class JwtUtils {

    static final Logger logger = LoggerFactory.getLogger(JwtUtils.class);

    public static String extractToken(String authHeaderValue) {

        if (StringUtils.isEmpty(authHeaderValue)) {
            throw new AuthenticationServiceException("Authorization header is empty.");
        }

        String tokenName = "Bearer";

        if (authHeaderValue.length() < tokenName.length()) {
            throw new AuthenticationServiceException("Invalid Authorization header size");
        }

        return authHeaderValue.substring(tokenName.length());
    }

    public static  UserDTO validate(String token, String signingKey, boolean isRefreshToken) {

        UserDTO userDTO = null;

        try {
            Claims body = Jwts.parser()
                    .setSigningKey(signingKey)
                    .parseClaimsJws(token)
                    .getBody();

            List<String> scopes = body.get("scopes", List.class);

            logger.info("scopes : {}, {} " , scopes, scopes.contains("ROLE_REFRESH"));

            if ((isRefreshToken && !scopes.contains("ROLE_REFRESH"))||
                    (!isRefreshToken && scopes.contains("ROLE_REFRESH"))) {
                throw new BadCredentialsException("Invalid JWT token");
            }

            userDTO = new UserDTO();
            userDTO.setUsername(body.getSubject());
        } catch (UnsupportedJwtException | MalformedJwtException | IllegalArgumentException | SignatureException e) {
            logger.info("Invalid JWT Token: {}",e);
            throw new BadCredentialsException("Invalid JWT token: ", e);
        } catch (ExpiredJwtException e) {
            logger.info("JWT Token expired: {}",e);
            throw new CredentialsExpiredException("JWT Token expired", e);
        }

        return userDTO;
    }
}
