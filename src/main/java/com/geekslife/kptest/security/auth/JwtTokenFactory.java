package com.geekslife.kptest.security.auth;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.geekslife.kptest.security.JwtSettings;
import com.geekslife.kptest.security.dto.UserDTO;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class JwtTokenFactory {

    static final Logger logger = LoggerFactory.getLogger(JwtTokenFactory.class);
    static final String SCOPE_ROLE_REFRESH = "ROLE_REFRESH";
    static final String SCOPE_ROLE_API = "ROLE_API";

    @Autowired
    JwtSettings jwtSettings;

    public String createToken(UserDTO userDTO) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();

        String accessToken = createJwtAccessToken(userDTO);
        String refreshToken = createRefreshToken(userDTO);

        Map<String, String> tokenMap = new HashMap<>();
        tokenMap.put("access_token", accessToken);
        tokenMap.put("refresh_token", refreshToken);

        String tokenList = mapper.writeValueAsString(tokenMap);

        return tokenList;
    }


    public String createJwtAccessToken(UserDTO userDTO) {

        LocalDateTime currentTime = LocalDateTime.now();

        Claims claims = Jwts.claims()
                .setSubject(userDTO.getUsername());
        claims.put("scopes", Arrays.asList(SCOPE_ROLE_API));

        String token = Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(Date.from(currentTime.atZone(ZoneId.systemDefault()).toInstant()))
                .setIssuer(jwtSettings.getTokenIssuer())
                .setExpiration(Date.from(currentTime
                        .plusMinutes(jwtSettings.getAccessTokenExpirationTime())
                        .atZone(ZoneId.systemDefault()).toInstant()))
                .signWith(SignatureAlgorithm.HS512, jwtSettings.getTokenSigningKey())
                .compact();

        return token;
    }

    private String createRefreshToken(UserDTO userDTO) {

        LocalDateTime currentTime = LocalDateTime.now();

        Claims claims = Jwts.claims()
                .setSubject(userDTO.getUsername());
        claims.put("scopes", Arrays.asList(SCOPE_ROLE_REFRESH));

        String token = Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(Date.from(currentTime.atZone(ZoneId.systemDefault()).toInstant()))
                .setIssuer(jwtSettings.getTokenIssuer())
                .setExpiration(Date.from(currentTime
                        .plusMinutes(jwtSettings.getRefreshTokenExpirationTime())
                        .atZone(ZoneId.systemDefault()).toInstant()))
                .signWith(SignatureAlgorithm.HS512, jwtSettings.getTokenSigningKey())
               .compact();

        return token;
    }
}