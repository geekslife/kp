package com.geekslife.kptest.security.auth;

import com.geekslife.kptest.security.JwtSettings;
import com.geekslife.kptest.security.model.JwtAuthenticationToken;
import com.geekslife.kptest.security.dto.UserDTO;
import com.geekslife.kptest.security.model.JwtUserDetails;
import com.geekslife.kptest.security.util.JwtUtils;
import io.jsonwebtoken.UnsupportedJwtException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component
public class JwtAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

    static final Logger logger = LoggerFactory.getLogger(JwtAuthenticationProvider.class);

    @Autowired
    JwtSettings jwtSettings;

    @Override
    protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {

        JwtAuthenticationToken jwtAuthenticationToken = (JwtAuthenticationToken) authentication;

        String token = jwtAuthenticationToken.getToken();

        logger.debug("token : {}, username : {}", token, username);

        UserDTO userDTO = JwtUtils.validate(token, jwtSettings.getTokenSigningKey(), false);

        if (userDTO == null) {
            throw new UnsupportedJwtException("JWT Token is incorrect");
        }

        return new JwtUserDetails(userDTO);
    }

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return (JwtAuthenticationToken.class.isAssignableFrom(authentication));
    }
}
