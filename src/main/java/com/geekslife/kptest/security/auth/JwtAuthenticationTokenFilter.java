package com.geekslife.kptest.security.auth;

import com.geekslife.kptest.security.WebSecurityConfig;
import com.geekslife.kptest.security.model.JwtAuthenticationToken;
import com.geekslife.kptest.security.util.JwtUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JwtAuthenticationTokenFilter extends AbstractAuthenticationProcessingFilter {

    static final Logger logger = LoggerFactory.getLogger(JwtAuthenticationTokenFilter.class);

    public JwtAuthenticationTokenFilter(boolean disable) {
        super("/");
    }

    public JwtAuthenticationTokenFilter() {
        super("/api/**");
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        super.successfulAuthentication(request, response, chain, authResult);
        chain.doFilter(request, response);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException, IOException, ServletException {

        String authHeaderValue = request.getHeader(WebSecurityConfig.AUTHORIZATION_HEADER_NAME);
        String authToken = JwtUtils.extractToken(authHeaderValue);

        logger.info("attemptAuthentication > authToken : {}", authToken);

        JwtAuthenticationToken token = new JwtAuthenticationToken(authToken);

        return getAuthenticationManager().authenticate(token);
    }
}