$(document).ready(function() {
    $('#disp-upload').click(function() {
        var display = $('#upload').css('display')
        if (display == 'block') {
            $('#upload').css('display', 'none')
            $('#disp-upload').text('보이기')
        } else {
            $('#upload').css('display', 'block')
            $('#disp-upload').text('숨기기')
        }
    });

    $('#disp-region').click(function() {
        var display = $('#region').css('display')
        if (display == 'block') {
            $('#region').css('display', 'none')
            $('#disp-region').text('보이기')
        } else {
            $('#region').css('display', 'block')
            $('#disp-region').text('숨기기')
        }
    });

    $('#btn-region').click(function() {
        $.ajax({
            url: '/api/v1/region/all',
            method: 'GET'
        }).done(function(data) {
            $('#region').val(JSON.stringify(data, null,4))
        })

    })

    $('#disp-support-info').click(function() {
        var display = $('#support-info').css('display')
        if (display == 'block') {
            $('#support-info').css('display', 'none')
            $('#disp-support-info').text('보이기')
        } else {
            $('#support-info').css('display', 'block')
            $('#disp-support-info').text('숨기기')
        }
    })

    $('#btn-support-info').click(function() {
        $.ajax({
            url: '/api/v1/support-info/all',
            method: 'GET'
        }).done(function(data) {
            $('#support-info').val(JSON.stringify(data,null,4))
        })
    })

    $('#btn-search').click(function() {
        var query = $('#search-input').val()
        data = JSON.stringify({ 'region' : query })

        $.ajax({
            url: '/api/v1/support-info/search',
            method: 'POST',
            data: data,
            dataType: 'json',
            contentType: 'application/json;charset=UTF-8'
        }).done(function(data) {
            console.log('data=>',data)
            $('#search-result').text(JSON.stringify(data, null, 4))
        })
    })

    $('#btn-update').click(function() {
        var query = $('#update-input').val()
        data = JSON.stringify(JSON.parse(query))

        $.ajax({
            url: '/api/v1/support-info/',
            method: 'POST',
            data: data,
            dataType: 'json',
            contentType: 'application/json;charset=UTF-8'
        }).done(function(data) {
            $('#update-result').text(JSON.stringify(data,null,4))
        })
    })

    $('#btn-sorted').click(function() {
        var K = $('#sorted-input').val()
        data = JSON.stringify( {"size":K})

        $.ajax({
            url: '/api/v1/region/sorted',
            method: 'POST',
            data: data,
            dataType: 'json',
            contentType: 'application/json;charset=UTF-8'
        }).done(function(data) {
            $('#sorted-result').text(JSON.stringify(data,null,4))
        })
    })

    $('#btn-min-rate').click(function() {
        $.ajax({
            url: '/api/v1/region/min-rate',
            method: 'POST'
        }).done(function(data) {
            $('#min-rate-result').text(JSON.stringify(data,null,4))
        })
    })
});