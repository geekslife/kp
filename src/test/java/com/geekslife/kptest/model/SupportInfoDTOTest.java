package com.geekslife.kptest.model;

import com.geekslife.kptest.dto.SupportInfoDTO;
import com.geekslife.kptest.service.DataService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SupportInfoDTOTest {

    @Autowired
    DataService DataService;

    @Test
    public void parseRate_단일퍼센트() {
        assertEquals(SupportInfoDTO.parseRate("3%"), Range.create(3.0f, 3.0f));
        assertEquals(SupportInfoDTO.parseRate("1.5%"), Range.create(1.5f, 1.5f));
    }

    @Test
    public void parseRate_구간퍼센트() {
        assertEquals(SupportInfoDTO.parseRate("3%~5%"), Range.create(3.0f, 5.0f));
        assertEquals(SupportInfoDTO.parseRate("0.3%~2.0%"), Range.create(0.3f, 2.0f));
        assertEquals(SupportInfoDTO.parseRate("1.5~2%"), Range.create(1.5f, 2.0f));
    }

    @Test
    public void parseRate_전액() {
        assertEquals(SupportInfoDTO.parseRate("대출이자 전액"), Range.create(100f, 100f));
    }

    @Test(expected = IllegalArgumentException.class)
    public void parseRate_예외() {
        SupportInfoDTO.parseRate("foo");
    }

    @Test
    public void parseLimit_추천금액_이내() {
        assertEquals(SupportInfoDTO.parseLimit("추천금액 이내"), 0);
    }

    @Test
    public void parseLimit_N억원_이내() {
        assertEquals(SupportInfoDTO.parseLimit("5억원 이내"), 500_000_000);
    }

    @Test
    public void foo() {
        Pattern pattern = Pattern.compile("(\\d+)(억|백만)원.*이내");
        Matcher m = pattern.matcher("5백만원이내");
        if(m.matches()) {
            System.err.println("=>"+m.group(0)+","+m.group(1));
        }
    }
}
