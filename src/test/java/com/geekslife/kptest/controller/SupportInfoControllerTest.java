package com.geekslife.kptest.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.geekslife.kptest.model.SupportInfo;
import com.geekslife.kptest.service.DataService;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SupportInfoControllerTest {

    @Autowired
    TestRestTemplate restTemplate;

    @Autowired
    DataService dataService;

    @Value("classpath:testdata.csv")
    Resource resourceFile;

    @Before
    public void setUp() throws IOException {
        dataService.deleteAll();
        assertEquals(98,dataService.importRegionInfo(resourceFile.getInputStream()));
        assertEquals(98,dataService.importSupportInfo(resourceFile.getInputStream()));
    }

    @Test
    public void 필수_지원하는_지자체_목록_검색_API() throws IOException {
        ResponseEntity<String> result = restTemplate.getForEntity("/api/v1/support-info/all", String.class);

        JsonNode root = new ObjectMapper().readTree(result.getBody());
        assertEquals(root.size(), 98);
        assertTrue(root.get(0).has("target"));
        assertTrue(root.get(0).has("region"));
        assertTrue(root.get(0).has("limit"));
    }



    @Test
    public void 필수_지원정보_수정_API() throws JSONException {

        SupportInfo info = dataService.findSupportInfoByRegionName("강원도");
        assertNotEquals(info.getUsage(),"테스트");

        JSONObject json = new JSONObject();
        json.put("region","강원도");
        json.put("usage","테스트");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> entity = new HttpEntity<>(json.toString() , headers);

        ResponseEntity<String> result = restTemplate.postForEntity("/api/v1/support-info/", entity,
                String.class);

        info = dataService.findSupportInfoByRegionName("강원도");
        assertEquals(info.getUsage(),"테스트");
    }

    @Test
    public void 필수_지자체명_입력받아_지원정보_출력하는_API() throws JSONException, IOException {
        JSONObject json = new JSONObject();
        json.put("region","강원도");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> entity = new HttpEntity<>(json.toString() , headers);

        ResponseEntity<String> result = restTemplate.postForEntity("/api/v1/support-info/search", entity,
                String.class);

        JsonNode root = new ObjectMapper().readTree(result.getBody());
        assertEquals(root.get("target").textValue(), "강원도 소재 중소기업으로서 강원도지사가 추천한 자");
        assertEquals(root.get("rate").textValue(), "3.00%~5.00%");
        assertEquals(root.get("limit").textValue(), "8억 이내");
    }

    @Test
    public void 필수_지자체목록_지원금액_내림차순_정렬() {

        long minValue =Long.MAX_VALUE;
        List<String> regionNames = dataService.findRegionByLimitAndRateWithTopN(98);
        for (String r : regionNames) {
            SupportInfo i = dataService.findSupportInfoByRegionName(r);
            assertTrue( i.getLimit() <= minValue );
            minValue = Math.min(minValue, i.getLimit());
        }
    }

    @Test
    public void 필수_지자체목록_지원금액_동일시_이차보전_평균_오름차순() {
        long lastLimit = 0;
        float lastRateValue = 100f;

        List<String> regionNames = dataService.findRegionByLimitAndRateWithTopN(98);
        for (String r : regionNames) {
            SupportInfo i = dataService.findSupportInfoByRegionName(r);
            if (i.getLimit() != lastLimit) {
                lastLimit = i.getLimit();
                lastRateValue = i.getRate().getAvg();
            } else {
                assertTrue( i.getRate().getAvg() >= lastRateValue );
                lastRateValue = i.getRate().getAvg();
            }
        }
    }
}
