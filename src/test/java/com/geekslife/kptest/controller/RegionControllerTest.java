package com.geekslife.kptest.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.geekslife.kptest.dto.SupportInfoDTO;
import com.geekslife.kptest.service.DataService;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.springframework.http.RequestEntity.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RegionControllerTest {
    @Autowired
    TestRestTemplate restTemplate;

    @Autowired
    DataService dataService;

    @Value("classpath:testdata.csv")
    Resource resourceFile;

    @Before
    public void setUp() throws IOException {
        dataService.deleteAll();
        assertEquals(98,dataService.importRegionInfo(resourceFile.getInputStream()));
        assertEquals(98,dataService.importSupportInfo(resourceFile.getInputStream()));
    }

    @Test
    public void listAllRegionInfo() throws IOException {

        ResponseEntity<String> result = restTemplate.getForEntity("/api/v1/region/all", String.class);

        JsonNode root = new ObjectMapper().readTree(result.getBody());
        assertEquals(root.size(), 98);
        assertTrue(root.get(0).has("regionCode"));
        assertTrue(root.get(0).has("regionName"));
    }

    @Test
    public void 필수_지원금액_이차보전_내림차순_K개_정렬후_출력() throws JSONException, IOException {
        JSONObject json = new JSONObject();
        json.put("size","11");

        String[] answer = {
                "경기도",
                "제주도",
                "국토교통부",
                "인천광역시",
                "안양시",
                "대구광역시",
                "안산시",
                "경상남도",
                "춘천시",
                "강원도",
                "태백시",
        };

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<String> entity = new HttpEntity<>(json.toString() , headers);

        ResponseEntity<String> result = restTemplate.postForEntity("/api/v1/region/sorted", entity,
                String.class);

        List<String> actual = new ArrayList<>();
        JsonNode root = new ObjectMapper().readTree(result.getBody());
        Iterator<JsonNode> it = root.iterator();
        while(it.hasNext()) {
            actual.add(it.next().textValue());
        }

        assertEquals(Arrays.asList(answer), actual);
    }

    @Test
    public void 필수_보전비율_가장_작은_추천기관() throws IOException {

        ResponseEntity<String> result = restTemplate.postForEntity("/api/v1/region/min-rate",
                new HttpEntity<>(""), String.class);

        assertEquals("안양상공회의소", result.getBody());
    }


    @Autowired
    RestTemplate testRestTemplate;

    @Test
    public void 옵션_추천() throws URISyntaxException, IOException {

        MockRestServiceServer mockServer = MockRestServiceServer.createServer(testRestTemplate);
        mockServer.expect(ExpectedCount.once(),
                requestTo(new URI("http://localhost:5000/api/v1/reco")))
                .andRespond(withStatus(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body("{\"region\":\"부천시\"}"))
        ;
        SupportInfoDTO dto = dataService.recommend("부천시 뭐에요.");
        mockServer.verify();

        assertEquals(dto.getRegion(),"부천시");
    }
}
