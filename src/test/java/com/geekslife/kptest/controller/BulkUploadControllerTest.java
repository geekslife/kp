package com.geekslife.kptest.controller;

import com.geekslife.kptest.service.DataService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BulkUploadControllerTest {

    @Autowired
    TestRestTemplate restTemplate;

    @Value("classpath:testdata.csv")
    Resource resourceFile;

    @Autowired
    DataService dataService;

    @Before
    public void setUp() {
        dataService.deleteAll();
    }

    @Test
    public void handleRegionInfo() {

        MultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();
        map.add("file", resourceFile);
        ResponseEntity<String> response = this.restTemplate.postForEntity("/api/v1/upload-region-info", map,
                String.class);

        assertEquals(response.getBody(), "98");
    }

    @Test
    public void 필수_데이터_파일에서_각_레코드를_데이터베이스에_저장하는_API_개발() {

        MultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();
        map.add("file", resourceFile);
        ResponseEntity<String> response = this.restTemplate.postForEntity("/api/v1/upload-support-info", map,
                String.class);

        assertEquals(response.getBody(), "98");
    }
}
