package com.geekslife.kptest.service;

import com.geekslife.kptest.model.Range;
import com.geekslife.kptest.model.SupportInfo;
import com.geekslife.kptest.dto.SupportInfoDTO;
import com.geekslife.kptest.repository.RegionInfoRepository;
import com.geekslife.kptest.repository.SupportInfoRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DataServiceTest {

    @Autowired
    DataService dataService;


    @Autowired
    RegionInfoRepository regionInfoRepository;

    @Autowired
    SupportInfoRepository supportInfoRepository;

    @Before
    public void setUp() {
        dataService.deleteAll();
    }

    private void setUpRegionInfoRepository(String line) throws IOException {
        dataService.importRegionInfo(
                new ByteArrayInputStream(line.getBytes(Charset.forName("UTF-8"))),
                false,
                Charset.forName("UTF-8"));
    }

    private SupportInfoDTO parseCsvLine(String line) throws IOException {
        List<SupportInfoDTO> dtoList = dataService.parseCsv(
                new ByteArrayInputStream(line.getBytes(Charset.forName("UTF-8"))),
                false,
                Charset.forName("UTF-8"));
        assertEquals(dtoList.size(), 1);
        return dtoList.get(0);
    }

    @Test
    public void parseCsv() throws IOException {

        String line = "1,강릉시,강릉시 소재 중소기업으로서 강릉시장이 추천한 자,운전,추천금액 이내,3%,강릉시,강릉지점,강릉시 소재 영업점\n";
        SupportInfoDTO dto = parseCsvLine(line);

        assertEquals(dto.getId(), "1");
        assertEquals(dto.getRegion(), "강릉시");
        assertEquals(dto.getTarget(), "강릉시 소재 중소기업으로서 강릉시장이 추천한 자");
        assertEquals(dto.getUsage(), "운전");
        assertEquals(dto.getLimit(), "추천금액 이내");
        assertEquals(dto.getRate(), "3%");
        assertEquals(dto.getInstitute(), "강릉시");
        assertEquals(dto.getMgmt(), "강릉지점");
        assertEquals(dto.getReception(), "강릉시 소재 영업점");
    }

    @Test
    public void importRegionInfo() throws IOException {

        String line = "2,강릉시,강릉시 소재 중소기업으로서 강릉시장이 추천한 자,운전,추천금액 이내,3%,강릉시,강릉지점,강릉시 소재 영업점\n";

        assertEquals(1, dataService.importRegionInfo(new ByteArrayInputStream(line.getBytes(Charset.forName("UTF-8"))), false, Charset.forName("UTF-8")));
        assertEquals("region_002", dataService.findRegionInfoByRegionName("강릉시").getRegionCode());
        assertEquals(0, dataService.importRegionInfo(new ByteArrayInputStream(line.getBytes(Charset.forName("UTF-8"))), false, Charset.forName("UTF-8")));
    }

    @Test
    public void convertSupportInfoDTD() throws IOException {

        String line = "2,강릉시,강릉시 소재 중소기업으로서 강릉시장이 추천한 자,운전,추천금액 이내,3%,강릉시,강릉지점,강릉시 소재 영업점\n";
        setUpRegionInfoRepository(line);

        SupportInfoDTO dto = parseCsvLine(line);

        SupportInfo info = dataService.convertSupportInfoDTO(dto);

        assertEquals(info.getId(), Long.valueOf(2));
        assertEquals(info.getLimit(), Long.valueOf(0));
        assertEquals(info.getRate(), Range.create(3.f, 3.f));
    }

    @Test
    public void convertSupportInfo() throws IOException {

        String line = "2,강릉시,강릉시 소재 중소기업으로서 강릉시장이 추천한 자,운전,추천금액 이내,3%,강릉시,강릉지점,강릉시 소재 영업점\n";
        setUpRegionInfoRepository(line);

        SupportInfoDTO dto = parseCsvLine(line);

        SupportInfo info = dataService.convertSupportInfoDTO(dto);

        SupportInfoDTO dto2 = dataService.convertSupportInfo(info);

        assertEquals(dto2.getId(), "2");
        assertEquals(dto2.getRegion(), "강릉시");
        assertEquals(dto2.getTarget(), "강릉시 소재 중소기업으로서 강릉시장이 추천한 자");
        assertEquals(dto2.getUsage(), "운전");
        assertEquals(dto2.getLimit(), "추천금액 이내");
        assertEquals(dto2.getRate(), "3.00%");  // 원천 3% --> 3.00% 로 변경됨
        assertEquals(dto2.getInstitute(), "강릉시");
        assertEquals(dto2.getMgmt(), "강릉지점");
        assertEquals(dto2.getReception(), "강릉시 소재 영업점");

    }
}
