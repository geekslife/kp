#
# testdata.csv 로부터 지자체명 정보를 받아와서 위경도 정보를 locations.csv 파일에 저장
#
import requests
import json
import os
import sys

def get_regions():
    pwd = os.path.dirname(os.path.realpath(__file__))
    csvpath = os.path.join(pwd,'..','src','test','resources','testdata.csv')
    f = open(csvpath)
    f.readline() #skip header
    regions = list([line.split(',')[1] for line in f.readlines()])
    print(regions)
    return regions

def query_latlon(location):
    API_KEY='19d2ae8f7255ed58293dab250593fc14'
    URL='https://dapi.kakao.com/v2/local/search/keyword.json'

    headers = { 'Authorization' : f'KakaoAK {API_KEY}'}
    resp = requests.get(URL, headers=headers, params={'query':location})
    data = json.loads(resp.text)['documents'][0]
    
    print(f'{location}: x={data["x"]} , y={data["y"]}')
    return (location,data['x'], data['y'])

if __name__=='__main__':
    result = []
    for r in get_regions():
        result.append(query_latlon(r))
    with open('locations.csv','w',encoding='utf-8') as fout:
        fout.write("지자체명,경도,위도\n")
        for it in result:
            fout.write(",".join(it)+"\n")
    