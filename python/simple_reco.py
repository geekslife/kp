import numpy as np
from scipy.spatial.distance import cosine
from gensim.models.keyedvectors import KeyedVectors, Word2VecKeyedVectors
from konlpy.tag import Okt as Twitter
from konlpy.tag import Okt as Twitter
from konlpy import jvm
import pickle
import re

twitter = Twitter()

class TooSimpleReco:
    def __init__(self):
        self.prepare()
    
    def preprocess(self, line):
        return re.sub("(,..)시",'\\1 시',line)

    def prepare(self):
        # 1. 각 row 에 대하여, term 을 뽑자
        term_freq = {} # '강릉시' -> { '강릉': 3 , ... }
        doc_freq = {} # '강릉': 4, ...
    
        with open('testdata.csv', encoding='euc-kr') as infile:
            infile.readline()
            for line in infile.readlines():
                name = line.split(',')[1]
                term_freq[name] = {}
                doc_terms = set()
                line = self.preprocess(line)
                for term in twitter.nouns(line):
                    if len(term)<=1: continue
                    if term in term_freq[name]:
                        term_freq[name][term] += 1
                    else:
                        term_freq[name][term] = 1
                    doc_terms.add(term)
                for doc_term in doc_terms:
                    if doc_term in doc_freq:
                        doc_freq[doc_term]+=1
                    else:
                        doc_freq[doc_term]=1
        self.regions_term_freq = term_freq
    
    def exec_reco(self, news_text):
        # 2. 뉴스로부터 특성을 매칭
        print("twitter=",twitter,",news=>",news_text)
        print('==============')
        news_tf = {}
        for noun in twitter.nouns(news_text):
            print(">",noun)
            if noun in news_tf : 
                news_tf[noun]+=1
            else:
                news_tf[noun]=1

        # 3. 매칭된 특성에 대해 csv 각 row 와의 유사도를 구해보자.
        result = {}
        for name, data_tf in self.regions_term_freq.items():
            s = self.similarity( data_tf, news_tf )
            #print(f"{name} => {s}")
            result[name]=s

        result = sorted(result.items(), key=lambda kv : (kv[1],kv[0]),reverse=True)
        return result[:3]

    def similarity( self, row_tf, news_tf ):
        # row_tf = { '강릉' : 3 , ... }
        # news_tf = { '철수' : 1 , ... }
        count = 0
        for t, c in news_tf.items():
            if t in row_tf:
                count += 1
        return count

        def idf(term):
            return 1/doc_freq[doc_term]


if __name__ == '__main__':
    recommender = TooSimpleReco()
    news = """철수는 충남 대천에 살고 있는데, 은퇴하고 시설 관리 비즈니스를 하기를 원한다. 시설 관리 관련 사업자들을 만나보니 관련 사업을 하려면 대체로 5 억은 필요하고, 이차보전 비율은 2% 이내가 좋다는 의견을 듣고 정부에서 운영하는 지자체 협약 지원정보를 검색한다."""
    recommender.exec_reco(news)
    #news = """철수는 청주에 살고 있는 장애인인데, 은퇴하고 시설 관리 비즈니스를 하기를 원한다. 시설 관리 관련 사업자들을 만나보니 관련 사업을 하려면 대체로 5 억은 필요하고, 이차보전 비율은 2% 이내가 좋다는 의견을 듣고 정부에서 운영하는 지자체 협약 지원정보를 검색한다."""

