from flask import Flask, request, jsonify
import simple_reco
import jpype

app = Flask(__name__)
recommender = simple_reco.TooSimpleReco()

@app.route('/api/v1/reco', methods=['POST'])
def recommend():
    jpype.attachThreadToJVM()
    content = request.json
    news_text = content['text']
    result = recommender.exec_reco(news_text)
    print('result=',result,',result_0=',result[0],',type=',type(result[0]))
    result_json = {
        'region':result[0][0]
    }
    return jsonify(result_json)

app.run(port=5000, debug=True)